#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <tf2_ros/buffer.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>


using namespace std;  

tf2_ros::Buffer buffer; 
tf2_ros::TransformListener *tf;
ros::Publisher initialpose_pub;
XmlRpc::XmlRpcValue artag_mapping;
std::string map_frame;
std::string base_link_frame;
double max_points_distance;
double max_radians_diff;
int setpose_interval_seconds;
time_t last_setpose_timestamp;

void arposemarker_callback(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{
   time_t now = time(0);
   if (now-last_setpose_timestamp < setpose_interval_seconds) return;
   if (msg->markers.size() > 0 && msg->markers[0].id != 0) 
   {  
     ROS_INFO("Marker id %d detected",msg->markers[0].id);
      //The frame you want to get the pose 
     std::stringstream out;
     out << msg->markers[0].id;
     std::string id_string = out.str();
     
     geometry_msgs::TransformStamped maptomarker_transform;
     try{
         maptomarker_transform = buffer.lookupTransform( map_frame,"ar_marker_"+ id_string,msg->markers[0].header.stamp);
         ROS_INFO("marker is at { %.2f; %.2f; %.2f; %.2f; %.2f; %.2f; %.2f}", 
            maptomarker_transform.transform.translation.x,
            maptomarker_transform.transform.translation.y,
            maptomarker_transform.transform.translation.z,
            maptomarker_transform.transform.rotation.x,
            maptomarker_transform.transform.rotation.y,
            maptomarker_transform.transform.rotation.z,
            maptomarker_transform.transform.rotation.w);     
     }
     catch (tf2::TransformException &ex) {
         ROS_ERROR_THROTTLE(1.0, "lookupTransform Error: %s\n", ex.what());
        
     }
     geometry_msgs::TransformStamped markertobaselink_transform;
     try{
         markertobaselink_transform = buffer.lookupTransform("ar_marker_"+ id_string,base_link_frame,msg->markers[0].header.stamp);
         ROS_INFO("marker to base_link is { %.2f; %.2f; %.2f; %.2f; %.2f; %.2f; %.2f}", 
            markertobaselink_transform.transform.translation.x,
            markertobaselink_transform.transform.translation.y,
            markertobaselink_transform.transform.translation.z,
            markertobaselink_transform.transform.rotation.x,
            markertobaselink_transform.transform.rotation.y,
            markertobaselink_transform.transform.rotation.z,
            markertobaselink_transform.transform.rotation.w);     
     }
     catch (tf2::TransformException &ex) {
         ROS_ERROR_THROTTLE(1.0, "lookupTransform Error: %s\n", ex.what());
        
     }
     bool mappingfound=false;
     double marker_px,marker_py,marker_pz,marker_qx,marker_qy,marker_qz,marker_qw;
     for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = artag_mapping.begin(); it != artag_mapping.end(); ++it)
     {
        std::string key = (std::string)(it->first);
        if (key == id_string)        {
           for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it2 = artag_mapping[it->first].begin(); it2 != artag_mapping[it->first].end(); ++it2)
           {
             for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it3 = artag_mapping[it->first][it2->first].begin(); it3 != artag_mapping[it->first][it2->first].end(); ++it3)
             {
               XmlRpc::XmlRpcValue val = artag_mapping[it->first][it2->first][it3->first];
               if((std::string)(it2->first) == "position")
               {
                 if((std::string)(it3->first) == "x")
                 { 
                    marker_px=static_cast<double>(val); 
                 }
                 else if ((std::string)(it3->first) == "y")
                 {
                   marker_py=static_cast<double>(val);
                 }
                 else if ((std::string)(it3->first) == "z")
                 {
                   marker_pz=static_cast<double>(val);
                 }
                 
               }
               else if ((std::string)(it2->first) == "orientation")
               {
                 if((std::string)(it3->first) == "x")
                 { 
                   marker_qx=static_cast<double>(val); 
                 }
                 else if ((std::string)(it3->first) == "y")
                 {
                   marker_qy=static_cast<double>(val); 
                 }
                 else if ((std::string)(it3->first) == "z")
                 {
                   marker_qz=static_cast<double>(val); 
                 }
                 else if ((std::string)(it3->first) == "w")
                 {
                   marker_qw=static_cast<double>(val); 
                 }
               }
             }                    
           }
           mappingfound=true;
        }        
     }
     if (!mappingfound) 
        return;
     ROS_INFO("mapping found { %.2f; %.2f; %.2f; %.2f; %.2f; %.2f; %.2f}", 
            marker_px,
            marker_py,
            marker_pz,
            marker_qx,
            marker_qy,
            marker_qz,
            marker_qw); 
     
     tf2::Quaternion rotation (marker_qx,marker_qy,marker_qz,marker_qw);
     tf2::Vector3 origin (marker_px,marker_py,marker_pz);
     tf2::Transform t1 (rotation, origin);
     
     // Compare between current and actual pose by checking distance and radians
     double curr_roll, curr_pitch, curr_yaw, actual_roll,actual_pitch,actual_yaw;
     double distance=sqrt(pow(marker_px-maptomarker_transform.transform.translation.x,2.0) + pow(marker_py-maptomarker_transform.transform.translation.y,2.0) + pow(marker_pz-maptomarker_transform.transform.translation.z,2.0));
     ROS_INFO("Distance between current and actual pose: %.2f",distance);
     
     tf2::Quaternion maptomarker_rotQ (maptomarker_transform.transform.rotation.x,maptomarker_transform.transform.rotation.y,maptomarker_transform.transform.rotation.z,maptomarker_transform.transform.rotation.w);
     tf2::Matrix3x3 maptomarker_rotMatrix(maptomarker_rotQ);
     maptomarker_rotMatrix.getRPY(curr_roll, curr_pitch, curr_yaw);
     tf2::Matrix3x3 actual_rotMatrix(rotation);
     actual_rotMatrix.getRPY(actual_roll, actual_pitch, actual_yaw);
     ROS_INFO("Radians between current and actual orientation(RPY): %.2f %.2f %.2f",actual_roll-curr_roll,actual_pitch-curr_pitch,actual_yaw-curr_yaw);
     if (distance<=max_points_distance && abs(actual_roll-curr_roll)<=max_radians_diff && abs(actual_pitch-curr_pitch)<=max_radians_diff && abs(actual_yaw-curr_yaw)<=max_radians_diff)
     {
        ROS_INFO("Do not need to set pose");
        return;
     }

     //ROS_INFO("RPY { %.2f ; %.2f ; %.2f}",roll,pitch,yaw);
     //3 lines below is to verify the conversion of RPY 
     //tf2::Quaternion myQuaternion;
     //myQuaternion.setRPY( roll, pitch, yaw ); 
     //ROS_INFO("Q { %.2f ; %.2f ; %.2f; %.2f}",myQuaternion[0],myQuaternion[1],myQuaternion[2],myQuaternion[3]); 


     tf2::Quaternion rotation1 (markertobaselink_transform.transform.rotation.x,markertobaselink_transform.transform.rotation.y,markertobaselink_transform.transform.rotation.z,markertobaselink_transform.transform.rotation.w);
     tf2::Vector3 origin1 (markertobaselink_transform.transform.translation.x,markertobaselink_transform.transform.translation.y,markertobaselink_transform.transform.translation.z);
     tf2::Transform t2 (rotation1, origin1);
     tf2::Transform baselinkPose = t1 * t2; // marker pose in the camera frame
     geometry_msgs::Transform transform_msg;
     tf2::convert(baselinkPose, transform_msg);
     ROS_INFO("base_link is at { %.2f; %.2f; %.2f; %.2f; %.2f; %.2f; %.2f}", 
            transform_msg.translation.x,
            transform_msg.translation.y,
            transform_msg.translation.z,
            transform_msg.rotation.x,
            transform_msg.rotation.y,
            transform_msg.rotation.z,
            transform_msg.rotation.w); 
     //set pose
     geometry_msgs::PoseWithCovarianceStamped initialpose;
     initialpose.header.frame_id = map_frame;
     initialpose.header.stamp = ros::Time::now();
     initialpose.pose.pose.position.x = transform_msg.translation.x;
     initialpose.pose.pose.position.y = transform_msg.translation.y;
     initialpose.pose.pose.position.z = transform_msg.translation.z;
     tf2::Quaternion quat (transform_msg.rotation.x,transform_msg.rotation.y,transform_msg.rotation.z,transform_msg.rotation.w);
     tf2::convert(quat, initialpose.pose.pose.orientation);    
     initialpose.pose.covariance[6*0+0] = 0.1;
     initialpose.pose.covariance[6*1+1] = 0.1;
     initialpose.pose.covariance[6*5+5] = 0.1;
     
     initialpose_pub.publish(initialpose);
     last_setpose_timestamp=now;
   }
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "ar_localization");
  ros::NodeHandle n;
  ros::Subscriber ar_marker_sub = n.subscribe("ar_pose_marker", 1000, arposemarker_callback);
  initialpose_pub = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 1);
  tf = new tf2_ros::TransformListener(buffer);  
  ros::param::get("~artag_mapping", artag_mapping);
  ros::param::param<std::string>("~map_frame", map_frame, "");
  ros::param::param<std::string>("~base_link_frame", base_link_frame, "");
  ros::param::param<double>("~max_points_distance", max_points_distance, 0.1);
  ros::param::param<double>("~max_radians_diff", max_radians_diff, 0.2);
  ros::param::param<int>("~setpose_interval_seconds", setpose_interval_seconds, 30);
  //ros::Publisher marker_pub = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("visualization_marker", 1);
  ros::spin();
  return 0;
}
